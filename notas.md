# Introduccion al Curso de Kotlin

## ¿Qué es Kotlin?

* Es un lenguaje que es estaticamente typeado(booleanos, enteros, strings, etc)
* Es Null Safety
* Corre sobre la JVM(Java Virtual Machine)
* 100% interoperable con Java
* Tiene caracteristicas de la programacion funcional

# Preparando el entorno de desarrollo para trabajar con Kotlin